# General settings (common between all environments):
from .base import *

# dev environment specific settings:

DATABASES['default']['HOST'] = 'localhost'
DATABASES['default']['NAME'] = 'dev_d_nashir'

DEBUG = True
ALLOWED_HOSTS = ['*']

# Uncomment to use sqlite
#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}

# More settings overrides for dev environment:
